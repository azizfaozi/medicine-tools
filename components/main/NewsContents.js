/* 
    author : Aziz Faozi
    filename : MainContent.js
    description : 
        maybe this code just show the main content, i planned this 
        section to make post like web blog.

*/

import React from 'react';

import { View, Text, Button, Image, StyleSheet, Alert } from 'react-native'
import { Card, ListItem,  Icon } from 'react-native-elements'

/* 
    this contents will get some news contents from server API 
    and provide it to display in android
*/

/* just variable testing  
    ini harusnya pake fetch api
*/

const users = [
  {
     name: 'brynn',
     avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg'
  }
 ]
   
const NewsContents =()=> {
    return (
        // implemented with Text and Button as children
        <View>
<Card title="CARD WITH DIVIDER">
  {
    users.map((u, i) => {
      return (
        <View key={i} style={styles.user}>
          <Image
            style={styles.image}
            resizeMode="cover"
            source={require('../images/pic2.jpg')}
          />
          <Text style={styles.name}>{u.name}</Text>
          <Button title="Baca dong"/>
          <Text>Ini harusnya ada spasi</Text>
        </View>
      );
    })
  }
</Card>


 <Card containerStyle={{padding: 0}} >
  {
    users.map((u, i) => {
      return (
        <ListItem
          key={i}
          roundAvatar
          title={u.name}
          avatar={{uri:u.avatar}}
        />
      );
    })
  }
</Card>



</View>
    );

}

const styles = StyleSheet.create({
    news: {
        padding: 10,
        marginVertical: 10,
        backgroundColor: '#ccc',
        borderColor: 'black',
        borderWidth:1
    
    }
})

export default NewsContents;