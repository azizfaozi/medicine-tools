import React from 'react';
import {Button, StyleSheet, Text, View, TextInput } from 'react-native';


/* For rendering signup layout */
const SignUpForm =()=>{
    return (
        <View style={styles.listItem}>
            <Text>Ayo daftarkan diri anda</Text>
            <TextInput placeholder="username" 
                sytle={{'border-color':'black', borderWidth: 1, padding: 10}}
            />
            <TextInput placeholder="nama" 
                sytle={{'border-color':'black', borderWidth: 1, padding: 10}}
            />
            <TextInput placeholder="alamat" 
                sytle={{'border-color':'black', borderWidth: 1, padding: 10}}
            />
            <TextInput placeholder="tempat lahir" 
                sytle={{'border-color':'black', borderWidth: 1, padding: 10}}
            />
            <TextInput placeholder="tanggal lahir" 
                sytle={{'border-color':'black', borderWidth: 1, padding: 10}}
            />
            <Button title="Daftar"></Button>
        </View>
    );

}

/* style form signup from */
const styles = StyleSheet.create({
    listItem: {
        padding: 10,
        marginVertical: 10,
        //backgroundColor: '#ccc',
        borderColor: 'black',
        borderWidth:1
    
    }

});
export default SignUpForm;