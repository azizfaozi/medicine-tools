/*
    author          : Aziz Amerul Faozi
    description     : ini adalah daftar screen.
*/


import React, {useState} from 'react';

import {
    Text,
    Button,
    ScrollView,
    View,
    TextInput,
    CheckBox,
    StyleSheet,
    TouchableOpacity

} from 'react-native';

/* add redux dependency */
import {useSelector, useDispatch} from 'react-redux';
/* action phone from reducer */
import {daftar, masuk} from '../../store/actions/phoneHandler'

const DaftarScreen =({navigation})=>{
    const phone_state = useSelector(state=>state.phoneRed);
    const dispatch = useDispatch();
    const [isChecked, setChecked]=useState(false);



    return (
        <ScrollView>
            <View style={{padding: 20, marginTop: 50}}>
                <View>
                <Text sytle={{marginTop:20}}>Nama Lengkap</Text>
                <TextInput
                    style={{marginVertical: 10, fontSize: 17}}
                    underlineColorAndroid='transparent'
                
                ></TextInput>
                </View>
                <View>
                    <Text>Nomer Handphone</Text>
                <View flexDirection="row" style={{marginTop:20, fontSize: 72}}>
                <Text style={styles.TeksInputPad}>+62 </Text>
                <TextInput
                    style={styles.TeksInputPad}
                    placeholder="81 1213412"
                    autoFocus
                    autoCompleteType="tel"
                    keyboardType="phone-pad"
                    textContentType="telephoneNumber"
                
                ></TextInput>
                </View>
                </View>

                <View flexDirection="row">
                    <CheckBox
                        value={isChecked}
                        onValueChange={setChecked}
                        
                        
                    />
                    <Text>Dengan ini saya setuju untuk menjadi bagian dari penelitian.</Text>
                </View>

                <View>
                <TouchableOpacity
                    style={isChecked?styles.buttonValid:styles.buttonInvalid}
                    disabled={!isChecked}
                    onPress={()=>navigation.navigate('Verifikasi')}
                >
                    <Text style={styles.TeksButton}>Daftar</Text>
                </TouchableOpacity>
                </View>
                <View>
                <Text>Sudah mendaftar? </Text>
                
                <TouchableOpacity
                        style={styles.buttonValid}
                        
                        onPress={()=>navigation.navigate('Beranda')}
                    >
                    <Text style={styles.TeksButton}>Masuk</Text>

                </TouchableOpacity>
                </View>
               
                <TouchableOpacity
                        style={styles.buttonValid}
                        
                        onPress={()=>dispatch(daftar("faoziaziz", "token", "08112134212"))}
                    >
                    <Text style={styles.TeksButton}>Check Dispatch</Text>

                </TouchableOpacity>

            </View>
            
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    TeksInputPad :{
        fontSize: 19
    },
    buttonValid: {
        alignItems: "center",
        backgroundColor: "green",
        padding: 10
    },
    buttonInvalid: {
        alignItems: "center",
        backgroundColor: "red",
        padding: 10
    },
    TeksButton: {
        fontSize: 19,
        color: "white"

    }
})

export default DaftarScreen;