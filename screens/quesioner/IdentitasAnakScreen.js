/* 
  author : Aziz Amerul Faozi
  description : this will show the checklist and quisioner form 
*/

import React, { useState, useEffect, useCallback, useReducer } from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  Platform,
  Alert,
  Button,
  Text,
  TextInput,
  Picker,
  KeyboardAvoidingView,
  ActivityIndicator
} from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { useSelector, useDispatch } from 'react-redux';

import HeaderButton from '../../components/UI/HeaderButton';
import * as postAction from '../../store/actions/checklist';
import Input from '../../components/UI/Input';
import Colors from '../../constants/Colors';
import Constants from 'expo-constants';
import RadioButton from '../../components/UI/RadioButton';


const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';
const PROP = [{
  key: 'laki-laki',
  text: 'Laki-laki'
}, {
  key: 'perempuan',
  text: 'Perempuan'
}]

const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value
    };
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid
    };
    let updatedFormIsValid = true;
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues
    };
  }
  return state;
};



const IdentitasAnakScreen = (props) => {
  /* This is some error methode */
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();
  const [isSignup, setIsSignup] = useState(false);
  const dispatch = useDispatch();

  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      telepon: '',
      alamat: '',
      nama: '',
      usia: '',
      kelamin: ''
    },
    inputValidities: {
      telepon: false,
      alamat: false,
      nama: false,
      usia: false,
      kelamin: false
    },
    formIsValid: false
  });

  useEffect(() => {
    if (error) {
      Alert.alert('An Error Occurred!', error, [{ text: 'Okay' }]);
    }
  }, [error]);
  
  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        value: inputValue,
        isValid: inputValidity,
        input: inputIdentifier
      });
    },
    [dispatchFormState]
  );

  const postHandler = async () => {
    
    let action;
    action = postAction.createPost(
      formState.inputValues.nama, 
      formState.inputValues.usia,
      formState.inputValues.telepon,
      formState.inputValues.alamat,
      formState.inputValues.kelamin
      );
    setError(null);
    setIsLoading(true);
    try {
      await dispatch(action);
      props.navigation.navigate('Beranda')
      console.log("simple")
    } catch (err) {
      console.log('mantan')
      setError(err.message);
      console.log("eror")
      setIsLoading(false);
    }
    setIsLoading(false);
  }


  return (
    <ScrollView>
      <Text>Identitas Anak</Text>
      
      <TextInput
        id="nama"
        label="nama"
        placeholder="check this man"
        onInputChange={inputChangeHandler}
       
        
      />
     
      <Input
        id="usia"
        label="usia"
        value="usia"
        keyboardType="decimal-pad"
        errorText="masukkan usia dengan benar"
        onInputChange={inputChangeHandler}
        initialValue=""

      />
      {/*
       <View>
      <Picker selectedValue={formState.inputValues.kelamin} onValueChange={(itemValue)}>
        <Picker.Item id="kelamin" label="laki-laki" value="laki-laki" />
        <Picker.Item id="kelamin" label="perempuan" value="perempuan" />
      </Picker>
      </View>
      */}
      <Input
        id="alamat"
        label="alamat"
        keyboardType="default"
        errorText="masukkan alamat dengan benar"
        onInputChange={inputChangeHandler}
        initialValue=""

      />

      <Input
        id="telepon"
        label="masukan nomer telefon anda"
        keyboardType="decimal-pad"
        errorText="masukkan nomer telefon dengan benar"
        onInputChange={inputChangeHandler}
        initialValue=""

      />

      <Button title="Submit" onPress={postHandler} />

    </ScrollView>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

export default IdentitasAnakScreen;
/* end of scroll */