/*
    author : Aziz Amerul Faozi
    description : this code for survey identitas anak
*/

import React, {Component} from 'react';
import {Platform, StyleSheet, Button, View, Text} from 'react-native';

class PolaPengasuhanScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            count: 0
        }
    }

    render (){
        return (
        <View>
            <Text>PolaPengasuhanScreen</Text>

        </View>);
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    toolbar: {
      marginTop: 30,
      backgroundColor: 'white',
      padding: 10,
      borderRadius: 5,
    },
    mediaPlayer: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      backgroundColor: 'black',
    },
  });

  export default PolaPengasuhanScreen;
