import * as React from 'react';
import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const InformasiScreen =({ navigation })=> {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Informasi Makanan dan Nutrisi Screen</Text>
      <Button
        title="Go to aktivitas"
        onPress={() => navigation.navigate('Aktivitas')}
      />
    </View>
  );
}

export default InformasiScreen;