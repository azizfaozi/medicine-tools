import * as React from 'react';
import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

/* this code should be access from global variable */
import {useSelector, useDispatch} from 'react-redux';

/* for testing */
import {increment, decrement} from '../../store/actions/counter';


const PemantauScreen =({ navigation })=> {
  const counter= useSelector(state=>state.counter);
  const dispatch = useDispatch();
  const isLogged = useSelector(state=>state.isLogged);

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Pemantau screen</Text>
      <Button
        title="Go to aktivitas"
        onPress={() => navigation.navigate('Aktivitas')}
      />
       <View>
        <Text>Test counter2 {counter}</Text>
        
        <Button
          title="+"
          onPress={()=>dispatch(increment())}
        />
        
        <Button
          title="-"
          onPress={()=>dispatch(decrement())}
        />
        <Text>Islogged {isLogged?"mantap":"mintip"}</Text>
       </View>

    </View>
  );
}

export default PemantauScreen;