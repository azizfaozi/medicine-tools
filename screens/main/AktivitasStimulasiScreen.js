import React, { Component } from 'react';
import { Platform, StyleSheet, Button, View, Text, Dimensions } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { ScrollView } from 'react-native-gesture-handler';
import { Video } from 'expo-av';

var video = [
  "https://d2b9mf76i0l1tj.cloudfront.net/mp4_v3/001.mp4",
  "https://d2b9mf76i0l1tj.cloudfront.net/mp4_v3/002.mp4",
  "https://d2b9mf76i0l1tj.cloudfront.net/mp4_v3/003.mp4",
  "https://d2b9mf76i0l1tj.cloudfront.net/mp4_v3/004.mp4",
  "https://d2b9mf76i0l1tj.cloudfront.net/mp4_v3/005.mp4",
  "https://d2b9mf76i0l1tj.cloudfront.net/mp4_v3/07.mp4",
  "https://d2b9mf76i0l1tj.cloudfront.net/mp4_v3/010.mp4",
  "https://d2b9mf76i0l1tj.cloudfront.net/mp4_v3/011.mp4",
  "https://d2b9mf76i0l1tj.cloudfront.net/mp4_v3/012.mp4"
]

var deskripsi =[
  "Check video 1",
  "Check video 2",
  "Judul video 003.mp4",
  "Judul video 004.mp4",
  "Judul video 005.mp4",
  "Judul video 07.mp4",
  "Judul video 010.mp4",
  "Judul video 011.mp4",
  "Judul video 012.mp4"
]

class AktivitasScreen extends Component {
  
  constructor(props){
    super(props);
    this.state = {
      count: 0
    }
  }


  render() {
    const { width } = Dimensions.get('window');
    const indexVideo = 6;
    const Description = "Untuk deskripsi"

    return (
      <View style={styles.container}>
        <Text style={{ textAlign: 'center' }}> Video {Math.abs(this.state.count)%9+1} </Text>
        <Video
          source={{ uri: video[Math.abs(this.state.count)%9]}}
          shouldPlay
          resizeMode="cover"
          style={{ width, height: 300 }}
        />
        <Text>{deskripsi[Math.abs(this.state.count)%9]}</Text>
        <Button onPress={()=>this.setState({count: this.state.count+1})} title="Lanjut"></Button>
        <Button onPress={()=>this.setState({count: this.state.count-1})} title="Kembali"></Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  toolbar: {
    marginTop: 30,
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 5,
  },
  mediaPlayer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'black',
  },
});
export default AktivitasScreen;
