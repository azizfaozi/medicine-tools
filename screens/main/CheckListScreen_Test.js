/* author : Aziz Amerul Faozi */
/* description : this will show the checklist and quisioner form */

import React, { useState, useEffect, useCallback, useReducer } from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  Platform,
  Alert,
  Button,
  Text,
  KeyboardAvoidingView,
  ActivityIndicator
} from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { useSelector, useDispatch } from 'react-redux';

import HeaderButton from '../../components/UI/HeaderButton';
import * as postAction from '../../store/actions/checklist';
import Input from '../../components/UI/Input';
import Colors from '../../constants/Colors';
import Constants from 'expo-constants';



const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    topik: 'Identitas Anak',
    title: 'First Item',
    pertanyaan: 'Nama bayi'
  }
];

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';

const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value
    };
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid
    };
    let updatedFormIsValid = true;
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues
    };
  }
  return state;
};


//https://stimulasi-anak.firebaseio.com/
/*
curl -X PUT -d '{ "first": "Jack", "last": "Sparrow" }' \
  'https://stimulasi-anak.firebaseio.com/users/jack/name.json'

  curl -X POST -d '{"user_id" : "jack", "text" : "Ahoy!"}' \
  'https://stimulasi-anak.firebaseio.com/message_list.json'

*/
const data = { mantap: 'excel3' };



const postToServer = (data) => {


  fetch('https://stimulasi-anak.firebaseio.com/check.json', {
    method: 'POST', // or 'PUT'
    headers: {
      'Content-Type': 'application/json',
    },



    body: JSON.stringify(data),
  })
    .then(response => response.json())
    .then(data => {
      console.log('Success:', data);
    })
    .catch((error) => {
      console.error('Error:', error);
    });

}


const CheckListScreen = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();
  const [isSignup, setIsSignup] = useState(false);
  const dispatch = useDispatch();

  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      telepon: '',
      alamat: '',
      nama: '',
      usia: '',
      kelamin: ''
    },
    inputValidities: {
      telepon: false,
      alamat: false,
      nama: false,
      usia: false,
      kelamin: false
    },
    formIsValid: false
  });

  useEffect(() => {
    if (error) {
      Alert.alert('An Error Occurred!', error, [{ text: 'Okay' }]);
    }
  }, [error]);
  
  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        value: inputValue,
        isValid: inputValidity,
        input: inputIdentifier
      });
    },
    [dispatchFormState]
  );

  const postHandler = async () => {
    console.log("test");
    let action;
    action = postAction.createPost(
      formState.inputValues.nama, 
      formState.inputValues.usia,
      formState.inputValues.telepon,
      formState.inputValues.alamat,
      formState.inputValues.kelamin
      );
    setError(null);
    setIsLoading(true);
    try {
      await dispatch(action);
      props.navigation.navigate('Beranda')
      console.log("simple")
    } catch (err) {
      console.log('mantan')
      setError(err.message);
      console.log("eror")
      setIsLoading(false);
    }
    setIsLoading(false);
  }


  return (
    <ScrollView>
      <Text>Identitas Anak</Text>
      
      <Input
        id="nama"
        label="nama"
        value="nama"
        keyboardType="default"
        autoCapitalize="none"
        errorText="Please enter a valid email address."
        onInputChange={inputChangeHandler}
        initialValue=""
      />
     
      <Input
        id="usia"
        label="usia"
        value="usia"
        keyboardType="decimal-pad"
        errorText="masukkan usia dengan benar"
        onInputChange={inputChangeHandler}
        initialValue=""

      />
      <Input
        id="kelamin"
        label="Jenis Kelamin 1 : untuk laki-laki 2 untuk perempuan"
        keyboardType="decimal-pad"
        errorText="masukkan angka yang tepat "
        onInputChange={inputChangeHandler}
        initialValue=""

      />
      
      <Input
        id="alamat"
        label="alamat"
        keyboardType="default"
        errorText="masukkan alamat dengan benar"
        onInputChange={inputChangeHandler}
        initialValue=""

      />

      <Input
        id="telepon"
        label="masukan nomer telefon anda"
        keyboardType="decimal-pad"
        errorText="masukkan nomer telefon dengan benar"
        onInputChange={inputChangeHandler}
        initialValue=""

      />

      <Button title="Submit" onPress={postHandler} />

    </ScrollView>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

export default CheckListScreen;