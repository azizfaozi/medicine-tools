/* author : Aziz Amerul Faozi */
/* description : this will show the checklist and quisioner form */

import * as React from 'react';
import { Button, StyleSheet, SafeAreaView, FlatList, View, Text, TextInput } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Constants from 'expo-constants';
import { ScrollView } from 'react-native-gesture-handler';

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    topik: 'Identitas Anak',
    title: 'First Item',
    pertanyaan: 'Nama bayi'
  }
];


const KuesionerPenelitian =()=>{

  return (
  <ScrollView>
    <Text>Identitas Anak</Text>
    <Text>Nama bayi : </Text>
    <TextInput/>
    <Text>Tanggal Lahir bayi : </Text>
    <TextInput/>
    <Text>Jenis Kelamin</Text>
    <TextInput/>
    <Text>Alamat </Text>
    <TextInput/>
    <Text>Telepon </Text>
    <TextInput/>

    <Text>Identitas dan Data Sosio-Ekonomi Orang Tua</Text>
    <Text>Nama Ayah : </Text>
    <TextInput/>
    <Text>Tanggal/Tanggal Lahir : </Text>
    <TextInput/>
    <Text>Alamat</Text>
    <TextInput/>
    <Text>Pendidikan terkahir ayah </Text>
    <TextInput/>
    <Text>Pekerjaan </Text>
    <TextInput/>
    <Text>Pendapatan </Text>
    <TextInput/>
    <Text>Lama bekerja/hari </Text>
    <TextInput/>
    <Text>Nama Ibu </Text>
    <TextInput/>
    <Text>Tempat/Tanggal Lahir</Text>
    <TextInput/>
    <Text>Alamat </Text>
    <TextInput/>
    <Text>Pendidikan terakhir ibu </Text>
    <TextInput/>
    <Text>Pekerjaan </Text>
    <TextInput/>
    <Text>Pendapatan </Text>
    <TextInput/>
    <Text>Lama ibu bekerja/hari </Text>
    <TextInput/>
    <Text>Kepemilikan rumah </Text>
    <TextInput/>
    <Text>Anggota serumah </Text>
    <TextInput/>
    <Text>Pengeluaran keluarga/bulan </Text>
    <TextInput/>
    <Text>Jumlah anak se rumah </Text>
    <TextInput/>

    <Text>Pemberian Imunisasi</Text>
    <Text>Riwayat imunisasi :  </Text>
    <TextInput/>
    <Text>Jenis imunisasi : </Text>
    <TextInput/>

    <Text>Data Keluarga</Text>
    <Text>Subyek merupakan anak ke  </Text>
    <TextInput/>
    <Text>dari</Text>
    <TextInput/> 
    <Text>bersaudara</Text>
    <Text>Jarak kelahiran dengan kakak sebelum subjek:</Text>
    <TextInput/>
    <Text>Anggota keuarga yang tinggal serumah : </Text>
    <Text>add some method to add family members </Text>

    <Text>Data Keluarga</Text>
    <Text>Subyek merupakan anak ke  </Text>
    <TextInput/>
    <Text>dari</Text>
    <TextInput/> 
    <Text>bersaudara</Text>
    <Text>Jarak kelahiran dengan kakak sebelum subjek:</Text>
    <TextInput/>
    <Text>Anggota keuarga yang tinggal serumah : </Text>
    <Text>add some method to add family members </Text>
    

  </ScrollView>
  );
}


const CheckListScreen =({ navigation })=> {
  
  return (
    <KuesionerPenelitian />
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

export default CheckListScreen;