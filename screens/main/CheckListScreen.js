/*
    author : Aziz Amerul Faozi
    description : this code for survey identitas anak
*/

import React, {Component} from 'react';
import {Platform, StyleSheet, Button, View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

class CheckListScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            count: 0
        }
    }

    render (){
        const { navigate } = this.props.navigation;
        return (
          <View>
        <View style={{flexDirection: 'row', flexWrap: 'wrap', marginHorizontal: 16, marginTop: 18}}>

          <View style={{justifyContent: 'space-between', flexDirection: 'row', width: '100%', marginBottom: 18}}>
            <View>
            <Icon name="scroll" size={58} color="white" onPress={() => navigate('ID_Anak')} />
            <Text style={{fontSize: 13, fontWeight: 'bold', textAlign: 'center'}}>Identitas Anak</Text>
            </View>
            <View>
            <Icon name="scroll" size={58} color="white" onPress={() => navigate('DataEkonomi')} />
            <Text style={{fontSize: 13, fontWeight: 'bold', textAlign: 'center'}}>Data Ekonomi</Text>
            </View>
            <View>
            <Icon name="scroll" size={58} color="white" onPress={() => navigate('Imunisasi')} />
            <Text style={{fontSize: 13, fontWeight: 'bold', textAlign: 'center'}}>Immunisasi</Text>
            </View>
            <View>
            <Icon name="scroll" size={58} color="white" onPress={() => navigate('DataKeluarga')} />
            <Text style={{fontSize: 13, fontWeight: 'bold', textAlign: 'center'}}>Data Keluarga</Text>
            </View>
          </View>

          <View style={{justifyContent: 'space-between', flexDirection: 'row', width: '100%', marginBottom: 18}}>
            <View>
            <Icon name="scroll" size={58} color="white" onPress={() => navigate('PolaPengasuhan')} />
            <Text style={{fontSize: 13, fontWeight: 'bold', textAlign: 'center'}}>Pengasuhan</Text>
            </View>
            <View>
            <Icon name="scroll" size={58} color="white" onPress={() => navigate('PertumbuhanBayi')} />
            <Text style={{fontSize: 13, fontWeight: 'bold', textAlign: 'center'}}>Pertumbuhan</Text>
            </View>
            <View>
            <Icon name="scroll" size={58} color="white" onPress={() => console.log("this clicked")} />
            <Text style={{fontSize: 13, fontWeight: 'bold', textAlign: 'center'}}>check click</Text>
            </View>
            <View>
            <Icon name="scroll" size={58} color="white" onPress={() => console.log("this clicked")} />
            <Text style={{fontSize: 13, fontWeight: 'bold', textAlign: 'center'}}>Check click</Text>
            </View>
           </View>

          </View>  

           
        </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    toolbar: {
      marginTop: 30,
      backgroundColor: 'white',
      padding: 10,
      borderRadius: 5,
    },
    mediaPlayer: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      backgroundColor: 'black',
    },
  });

  export default CheckListScreen;
