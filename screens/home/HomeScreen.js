/*
    author : Aziz Amerul Faozi
    description : ini adalah homescreen
*/

import * as React from 'react';
import { Button, View, Text, TextInput, Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { ScrollView } from 'react-native-gesture-handler';

/*  this code will include some action reducers */
//import '../../store/actions/userdata'
/* import reducers*/
//import '../../store/reducers/userdata'

/* this code should be access from global variable */
import {useSelector, useDispatch} from 'react-redux';

/* for testing */
import {increment, decrement} from '../../store/actions/counter';


/* HomeScreen */
const HomeScreen = ({ navigation }) => {

  const counter= useSelector(state=>state.counter);
  const auth = useSelector(state=>state.auth)
  const phoneAuth = useSelector(state=>state.phoneRed)
  const dispatch = useDispatch();

  return (
    
    <View style={{ flex: 1, marginHorizontal:17, marginVertical:13}}>
      <ScrollView>
      <View flexDirection='row'
        style={{ 
          paddingTop: 20, 
          paddingBottom: 14, 
          justifyContent: 'center', 
          backgroundColor: '#2F65BD',
          borderTopLeftRadius:5,
          borderTopRightRadius:5,
          borderBottomEndRadius: 5,
          borderBottomRightRadius: 5
          }}>
        <View style={{ flex: 1, alignItems: 'center' }}>
          <Icon name="rocket" size={40} color="white" onPress={() => navigation.navigate('Aktivitas')} />
          <Text style={{fontSize:13, fontWeight: 'bold', color: 'white'}}>Aktivitas</Text>
        </View>
        <View style={{ flex: 1, alignItems: 'center' }}>
          <Icon name="calendar-check" size={40} color="white" onPress={() => navigation.navigate('CheckList')} />
          <Text style={{fontSize:13, fontWeight: 'bold', color: 'white'}}>Kuesioner</Text>
        </View>
        <View style={{ flex: 1, alignItems: 'center' }}>
          <Icon name="shoe-prints" size={40} color="white" onPress={() => navigation.navigate('Pemantau')} />
          <Text style={{fontSize:13, fontWeight: 'bold', color: 'white'}}>Pemantau</Text>
        </View>
        <View style={{ flex: 1, alignItems: 'center' }}>
          <Icon name="scroll" size={40} color="white" onPress={() => navigation.navigate('Informasi')} />
          <Text style={{fontSize:13, fontWeight: 'bold', color: 'white', paddingHorizontal:10}}>Infomasi</Text>
        </View>
      </View>

      <View style={{height:17, backgroundColor: '#F2F2F4'}}
      >

      </View>
      {/* News section*/}
      <View>
        <View>

        </View>
      </View>


      <View style={{ flex: 1, backgroundColor: 'green', alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>

      {/* this code will be use for testing screen */}
      
       <View>
        <Text>Test counter {counter}</Text>
        <Text>Test userid {auth.userId}</Text>
        <Text>test phone {phoneAuth.phoneNumber}</Text>
        
        <Button
          title="+"
          onPress={()=>dispatch(increment())}
        />
        
        <Button
          title="-"
          onPress={()=>dispatch(decrement())}
        />
       </View>

      </View>
      </ScrollView>
      <View style={{ height: 54, flexDirection: 'row' }}>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Icon name="home" size={26} color="#900" onPress={() => navigation.navigate('Beranda')} />
          <Text>Home</Text>
        </View>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Icon name="rocket" size={26} color="#000" onPress={() => navigation.navigate('Aktivitas')} />
          <Text>Aktivitas</Text>
        </View>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Icon name="hands-helping" size={26} color="#000" onPress={() => navigation.navigate('NotImplement')} />
          <Text>Help</Text>
        </View>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Icon name="snowman" size={26} color="#000" onPress={() => navigation.navigate('Auth')} />
          <Text>Inbox</Text>
        </View>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Icon name="mandalorian" size={26} color="#000" onPress={() => navigation.navigate('AuthPhone')} />
          <Text>Account</Text>
        </View>

      </View>
    </View>
  );
}

export default HomeScreen;