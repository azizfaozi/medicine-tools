import { AppLoading, SplashScreen, Updates } from 'expo';
import { Asset } from 'expo-asset';
import Constants from 'expo-constants';
import React from 'react';
import {Components} from 'react';
import { Animated, Button, StyleSheet, Text, View, TextInput, Modal, ImagePropTypes } from 'react-native';
import SignUpForm from '../components/main/SignUpForm';
import SignInFom from '../components/main/SignInForm';
import Header from '../components/base/Header.js';
import NewsContents from '../components/main/NewsContents';

/* this will be main screen ?? maybe */
const MainScreen=()=> {
return (
    <View style={styles.screen}>
    <Header title="Medical Tools"></Header>
    <SignInFom />
    <SignUpForm />
    <View
    style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }}>
    
    <Text
        style={{
        color: 'black',
        fontSize: 30,
        marginBottom: 15,
        fontWeight: 'bold',
        }}>
        Medical Tool 
    </Text>
    
    

    <TextInput 
        placeholder="check" 
        sytle={{'border-color':'black', borderWidth: 1, padding: 10}}
    />
    <Button title="Check MedTools" onPress={() => Updates.reload()} />
    </View>
    </View>
);
}

const styles = StyleSheet.create({
    screen: {
        flex: 1
    }
})
  
export default MainScreen;
