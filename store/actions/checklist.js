/* this code use for post to firebase */

export const CHECKLIST = 'CHECKLIST';

export const createPost = (nama, usia, telepon, alamat, kelamin) => {
  return async dispatch => {
    const response = await fetch(
      'https://stimulasi-anak.firebaseio.com/checklist.json',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          id: 'sampah',
          nama: nama,
          usia: usia,
          telepon: telepon,
          alamat: alamat,
          kelamin: kelamin
        })
      }
    );

    if (!response.ok) {
        const errorResData = await response.json();
        const errorId = errorResData.error.message;
        let message = 'Something went wrong!';
        throw new Error(message);
      }

    const resData = await response.json();
    console.log(resData);
    dispatch({ type: CHECKLIST });
  };
};

