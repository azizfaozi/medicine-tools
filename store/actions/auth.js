/*
  Action untuk mengatur masuk :

  Issue : 
    1. code masih dalam masalah dia tidak bisa save email.
    2. Belum login untuk nomer hp
*/

import {AsyncStorage} from 'react-native';

export const SIGNUP = 'SIGNUP';
export const LOGIN = 'LOGIN';
export const AUTHENTICATE='AUTHENTICATE'
export const LOGOUT='LOGOUT'


let timer;

export const authenticate = (userId, token, expiryTime)=>{
  return dispatch => {
    dispatch(setLogoutTimer(expiryTime));
    dispatch({type:AUTHENTICATE, userId:userId, token: token});
  
  }
}

export const signup = (email, password) => {
  return async (dispatch, getState) => {
    console.log(getState())
    const response = await fetch(
      'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyDnQ8k2_mKjsn7MiusSZt57UZxiS2lecrA',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: email,
          password: password,
          returnSecureToken: true
        })
      }
    );

    if (!response.ok) {
        const errorResData = await response.json();
        const errorId = errorResData.error.message;
        let message = 'Hmm ada yang salah!';
        if (errorId === 'EMAIL_EXISTS') {
          message = 'Email sudah terdaftar';
        }
        throw new Error(message);
      }

    const resData = await response.json();
    console.log(resData);
    dispatch(authenticate(
      resData.localId,
      resData.idToken,
      parseInt(resData.expiresIn) * 1000
    )
    );

    /* add expiration date */
    const expirationDate = new Date(new Date().getTime()+parseInt(resData.expiresIn)*1000);
    /* save data to async storage */
    saveDataToStorage(resData.idToken, resData.localId, expirationDate);
  };
};

export const login = (email, password) => {
  return async dispatch => {
    const response = await fetch(
      'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDnQ8k2_mKjsn7MiusSZt57UZxiS2lecrA',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: email,
          password: password,
          returnSecureToken: true
        })
      }
    );

    /* mendapat response error dari server */
    if (!response.ok) {
      const errorResData = await response.json();
      const errorId = errorResData.error.message;
      let message = 'Something went wrong!';
      if (errorId === 'EMAIL_NOT_FOUND') {
        message = 'Email belum terdaftar.';
      } else if (errorId === 'INVALID_PASSWORD') {
        message = 'Password tidak cocok';
      }
      throw new Error(message);
    }

    const resData = await response.json();
    console.log(resData);
    dispatch(  authenticate(
      resData.localId,
      resData.idToken,
      parseInt(resData.expiresIn) * 1000
    ));
    /* add expiration date */
    const expirationDate = new Date(new Date().getTime()+parseInt(resData.expiresIn)*1000);
    /* save data to async storage */
    saveDataToStorage(resData.idToken, resData.localId, expirationDate);
  };
};

export const logout=()=>{
  clearLogoutTimer();
  AsyncStorage.removeItem('userData');
  return {type: LOGOUT}
}

const clearLogoutTimer = ()=>{
  if(timer){
    clearTimeout(timer);
  }

}

const setLogoutTimer = expirationTime=>{
  return dispatch => {
    timer = setTimeout(()=>{
      dispatch(logout());
    }, expirationTime);
  }
}

const saveDataToStorage = (token, userId, expirationDate) => {
  AsyncStorage.setItem(
    'userData',
    JSON.stringify({
      token: token,
      userId: userId,
      expiryDate: expirationDate.toISOString()
    })
  )
}