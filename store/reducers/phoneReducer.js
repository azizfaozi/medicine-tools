import {
    DAFTAR, 
    MASUK, 
    daftar, 
    masuk
} from '../actions/phoneHandler';

const initialState = {
    token: null,
    userId: null,
    phoneNumber: null
}

const phoneReducer =(state=initialState, action)=>{
    switch(action.type){
        case DAFTAR:
            return {
                token: action.token,
                userId: action.userId,
                phoneNumber: action.phoneNumber
            }
        case MASUK:
            return {
                token: action.token,
                userId: action.userId,
                phoneNumber: action.phoneNumber
            } 
        default:
            return state
    }
}

export default phoneReducer;