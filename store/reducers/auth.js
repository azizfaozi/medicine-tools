/*
    author  : Aziz Amerul Faozi
    desk    : this code will store authentication methode

*/
import {AUTHENTICATE, LOGOUT} from '../actions/auth';
/*
    token digunakan untuk ??

*/

const initialState = {
    token: null,
    userId: null
}

export default (state=initialState, action)=>{
    switch (action.type){
        case AUTHENTICATE:
            return {
                token: action.token,
                userId: action.userId
            }
        case LOGOUT:
            return initialState;
        default:
            return state;
    }
}