/*
    author      : Aziz Amerul Faozi
    Description : Ini readucer untuk set state data

    export const ADD_USERDATA = 'ADD_USERDATA';
    export const DEL_USERDATA = 'DEL_USERDATA';
    export const UPDATE_USERDATA = 'UPDATE_USERDATA';

*/

import {
    ADD_USERDATA,
    DEL_USERDATA,
    UPDATE_USERDATA
} from '../actions/userdata';

function userReducer (state=[], action){
  switch(action.type){
        case ADD_USERDATA:
          return [
              ...state,
              {
                  id: "identites",
                  description: action.payload.description,
                  resolved: false
              }
          ];
        case DEL_USERDATA:
            return state
        case UPDATE_USERDATA:
            return state 
        default:
            return state
    
  }
}

export default userReducer;