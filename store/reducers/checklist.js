/*
    author  : Aziz Amerul Faozi
    desc    : This code to implement redux on Checklist 
*/

import CHECKLIST from '../../data/dummy-checklist';
import {
  DELETE_CHECKLIST,
  CREATE_CHECKLIST,
  UPDATE_CHECKLIST
} from '../actions/checklist';


import Checklist from '../../models/checklist';

const initialState = {
  availableChecklist: CHECKLIST,
  userChecklist: CHECKLIST.filter(check => check.id === 'p1')
};
// namabayi,tanggallahirbayi, jeniskelaminbayi, alamatbayi, teleponbayi
export default (state = initialState, action) => {
  switch (action.type) {
    case CREATE_CHECKLIST:
      const newChecklist = new Checklist(
        action.checklistData.id,
        'u1',
        action.checklistData.namabayi,
        action.checklistData.tanggallahirbayi,
        action.checklistData.jeniskelaminbayi,
        action.checklistData.alamatbayi,
        action.checklistData.teleponbayi
      );
      return {
        ...state,
        availableChecklist: state.availableChecklist.concat(newChecklist),
        userChecklist: state.userChecklist.concat(newChecklist)
      };
    case UPDATE_CHECKLIST:
      const checklistIndex = state.userChecklist.findIndex(
        check => check.id === action.pid
      );
        // namabayi,tanggallahirbayi, jeniskelaminbayi, alamatbayi, teleponbayi
      const updatedChecklist = new Checklist(
        action.pid,
        state.userChecklist[checklistIndex].userId,
        action.checklistData.namabayi,
        action.checklistData.tanggallahirbayi,
        action.checklistData.jeniskelaminbayi,
        action.checklistData.jeniskelaminbayi,
        action.checklistData.alamatbayi,
        action.checklistData.teleponbayi
      );
      const updatedUserChecklist = [...state.userChecklist];
      updatedUserChecklist[checklistIndex] = updatedChecklist;
      const availableChecklistIndex = state.availableChecklist.findIndex(
        prod => prod.id === action.pid
      );
      const updatedAvailableChecklist = [...state.availableChecklist];
      updatedAvailableChecklist[availableChecklistIndex] = updatedChecklist;
      return {
        ...state,
        availableChecklist: updatedAvailableChecklist,
        userChecklist: updatedUserChecklist
      };

    case DELETE_CHECKLIST:
      return {
        ...state,
        userChecklist: state.userChecklist.filter(
          checklist => checklist.id !== action.pid
        ),
        availableChecklist: state.availableChecklist.filter(
          checklist => checklist.id !== action.pid
        )
      };
  }
  return state;
};