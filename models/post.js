class Post {
    constructor(id, ownerId, author, title, content, description){
        this.id=id;
        this.ownerId=ownerId;
        this.author=author;
        this.title=title;
        this.content=content;
        this.description=description;
    }
}