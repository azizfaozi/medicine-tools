/* 
    author      : Aziz Amerul Faozi
    description : this is for model Checklist. 
*/

class Checklist {
    constructor(id, namabayi, tanggallahirbayi, jeniskelaminbayi, alamatbayi, teleponbayi) {
      this.id = id;
      this.namabayi = namabayi;
      this.tanggallahirbayi = tanggallahirbayi;
      this.jeniskelaminbayi = jeniskelaminbayi;
      this.alamatbayi = alamatbayi;
      this.teleponbayi = teleponbayi;
    }
  }
  
  export default Checklist;
  