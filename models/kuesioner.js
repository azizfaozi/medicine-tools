/* 
    author      : Aziz Amerul Faozi
    description : this is for model quisioner. 
*/

class Kuesioner {
    constructor(id, namabayi, tanggallahirbayi, jeniskelaminbayi, alamatbayi, teleponbayi) {
      this.id = id;
      this.namabayi = namabayi;
      this.tanggallahirbayi = tanggallahirbayi;
      this.jeniskelaminbayi = jeniskelaminbayi;
      this.alamatbayi = alamatbayi;
      this.teleponbayi = teleponbayi;
    }
  }
  
  export default Kuesioner;
  