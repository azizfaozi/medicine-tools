import * as React from 'react';
import { Platform } from 'react-native';
import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AuthScreen from '../screens/user/AuthScreen';
import Colors from '../constants/Colors';


const AuthStack = createStackNavigator();


const AuthNavigator =()=>{
    return (

        <AuthStack.Navigator
        initialRouteName="Auth"
        screenOptions={{ gestureEnabled: false }}
      >
        <AuthStack.Screen
          name="Auth"
          component={AuthScreen}
          options={{ title: 'Authen' }}
        />
       
      </AuthStack.Navigator>

    );
}


export default AuthNavigator;
