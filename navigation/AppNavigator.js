/*
  username    : Aziz Amerul Faozi
  description : This code will used for user navigation.

*/

import * as React from 'react';
import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


/* some default screen */
import HomeScreen from '../screens/home/HomeScreen';
import AktivitasScreen from '../screens/main/AktivitasStimulasiScreen';
import CheckListScreen from '../screens/main/CheckListScreen';
import InformasiScreen from '../screens/main/InformasiMakananNutrisiScreen';
import PemantauScreen from '../screens/main/PemantauScreen';
import TanyaJawabScreen from '../screens/main/TanyaJawabScreen';
import AuthScreen from '../screens/user/AuthScreen';
import EditProductScreen, {  screenOptions as editProductScreenOptions} from '../screens/user/EditProductScreen';


/* some auth revision */
import DaftarScreen from '../screens/user/DaftarScreen';

/* Part of quesioner */

import IdentitasAnakScreen from '../screens/quesioner/IdentitasAnakScreen';
import DataEkonomiScreen from '../screens/quesioner/DataEkonomiScreen';
import DataKeluargaScreen from '../screens/quesioner/DataKeluargaScreen';
import PertumbuhanBayiScreen from  '../screens/quesioner/PertumbuhanBayiScreen';
import ImunisasiScreen from '../screens/quesioner/ImunisasiScreen';
import PolaPengasuhanScreen from '../screens/quesioner/PolaPengasuhanScreen';

/* part of not implement */

import NotImplement from '../screens/NotImplement';

/* phone authentication */
import AuthPhoneScreen from '../screens/user/AuthPhoneScreen';
import VerifikasiScreen from '../screens/user/VerifikasiScreen';
const Stack = createStackNavigator();

function AppNavigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Daftar">
        <Stack.Screen 
          name="Beranda" 
          component={HomeScreen}
          options={{ title: 'Beranda' }}
          />
        <Stack.Screen 
          name="Aktivitas" 
          component={AktivitasScreen} 
          options={{ title: 'Aktivitas' }}
          />
        <Stack.Screen 
          name="CheckList" 
          component={CheckListScreen} 
          options={{ title: 'Kuesioner' }} 
          />
        <Stack.Screen 
          name="Informasi" 
          component={InformasiScreen}
          options={{title: 'Informasi'}} 
          />
        <Stack.Screen 
          name="Pemantau" 
          component={PemantauScreen} 
          options={{title: 'Pemantau'}}
          />
        <Stack.Screen 
          name="EditProduct" 
          component={EditProductScreen} 
          options={editProductScreenOptions} />
        <Stack.Screen 
          name="Tanya" 
          component={TanyaJawabScreen}
          options={{title: 'Tanya Jawab'}} 
          />

        <Stack.Screen 
          name="Verifikasi" 
          component={VerifikasiScreen}
          options={{title: 'Verifikasi Code'}} 
          />
        <Stack.Screen 
          name="Auth" 
          component={AuthScreen}
          options={{title: 'Masuk'}} 
          />

        <Stack.Screen 
          name="ID_Anak" 
          component={IdentitasAnakScreen} 
          options={{ title: 'Identitas Anak' }}/>

        <Stack.Screen 
          name="DataEkonomi" 
          component={DataEkonomiScreen} 
          options={{ title: 'Data Ekonomi' }}/>

        <Stack.Screen 
          name="DataKeluarga" 
          component={DataKeluargaScreen} 
          options={{ title: 'Data Keluarga' }}/>
          
        <Stack.Screen 
          name="PertumbuhanBayi" 
          component={PertumbuhanBayiScreen} 
          options={{ title: 'Pertumbuhan Bayi' }}/>

        <Stack.Screen 
          name="Imunisasi" 
          component={ImunisasiScreen} 
          options={{ title: 'Imunisasi' }} />

        <Stack.Screen 
          name="PolaPengasuhan" 
          component={PolaPengasuhanScreen} 
          options={{ title: 'Pola Pengasuhan' }} />

        <Stack.Screen
          name="NotImplement"
          component={NotImplement}
          options={{title: "Dalam pengembangan"}}
        />

        <Stack.Screen
          name="AuthPhone"
          component={AuthPhoneScreen}
          options={{title: "Masuk"}}
        />

        <Stack.Screen
          name="Daftar"
          component={DaftarScreen}
          options={{title: "Daftar"}}
        />
        
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default AppNavigator;