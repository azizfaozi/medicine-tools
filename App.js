
/* 
    author  : Aziz Amerul Faozi
    desc    : this is will set the app code
*/


import { createStackNavigator } from '@react-navigation/stack';

import React, { useState } from 'react';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { AppLoading } from 'expo';
import * as Font from 'expo-font';
import ReduxThunk from 'redux-thunk';

/* all reducer */
import productsReducer from './store/reducers/products';
import cartReducer from './store/reducers/cart';
import ordersReducer from './store/reducers/orders';
import checklistReducer from './store/reducers/checklist';
import authReducer from './store/reducers/auth'
/* navigator */
import AppNavigator from './navigation/AppNavigator'


/* test reducers */
import counterReducer from './store/reducers/counter';
import loggedReducer from './store/reducers/isLogged';
import phoneReducer from './store/reducers/phoneReducer'


const Stack = createStackNavigator();


const rootReducer = combineReducers({
  products: productsReducer,
  cart: cartReducer,
  orders: ordersReducer,
  checklist: checklistReducer,
  counter: counterReducer,
  isLogged: loggedReducer,
  auth: authReducer,
  phoneRed: phoneReducer


});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

const fetchFonts = () => {
  return Font.loadAsync({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
  });
};

export default function App() {
  /*
    this code of Applications
  */
  const [fontLoaded, setFontLoaded] = useState(false);

  if (!fontLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => {
          setFontLoaded(true);
        }}
      />
    );
  }

  return (
    <Provider store={store}>
      <AppNavigator />
    </Provider>
  );
}